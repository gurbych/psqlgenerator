from yaml import load
from yaml.parser import ParserError
from Templates import (
    CREATE_TABLE,
    CREATE_TRIGGER,
    ALTER_TABLE,
    CREATE_RELATION_TABLE
)

class Generator(object):
    def __init__(self):
        self.create_statements = set()
        self.alter_statements = set()
        self.triggers = set()

    def load(self, file_in):
        try:
            with open(file_in, 'r') as f:
                self.yaml = load(f)
        except (EnvironmentError, ParserError) as e:
            print e

    def generate_sql(self):
        self.__generate_tables()
        self.__generate_triggers()
        self.__build_relations()

    def write(self, file_out):
        try:
            with open(file_out, 'w') as f:
                f.write('\n'.join(self.create_statements))
                f.write('\n')
                f.write('\n'.join(self.alter_statements))
                f.write('\n')
                f.write('\n'.join(self.triggers))
        except (EnvironmentError) as e:
            print e

    def clear(self):
        self.create_statements.clear()
        self.alter_statements.clear()
        self.triggers.clear()

    def __generate_tables(self):
        for table_iter in self.yaml:
            table = table_iter.lower()
            columns = []
            for column in self.yaml[table_iter]['fields']:
                columns.append('{}_{} {} NOT NULL,'.format(table, column, self.yaml[table_iter]['fields'][column]))
            self.create_statements.add(CREATE_TABLE.format(table=table, columns='\n    '.join(columns)))

    def __build_relations(self):
        for entity in self.yaml:
            for relation, relation_type in self.yaml[entity]['relations'].iteritems():
                reverted_relation = self.yaml[relation]['relations'][entity]
                if reverted_relation == "many":
                    if relation_type == "one":
                        self.__many_to_one(entity, relation)
                    if relation_type == "many":
                        self.__many_to_many(entity, relation)

    def __many_to_one(self, many, one):
        self.alter_statements.add(ALTER_TABLE.format(this=many.lower(), referenced=one.lower()))

    def __many_to_many(self, left, right):
        left = left.lower()
        right = right.lower()
        table_names = [left, right]
        table_names. sort()
        self.create_statements.add(CREATE_RELATION_TABLE.format(left_table=table_names[0], right_table=table_names[1]))
        relation_table = '__'.join(table_names)
        self.__many_to_one(relation_table, table_names[0])
        self.__many_to_one(relation_table, table_names[1])

    def __generate_triggers(self):
        for table in self.yaml:
            self.triggers.add(CREATE_TRIGGER.format(table.lower()))

if __name__ == '__main__':
    g = Generator()
    g.load('schema.yaml')
    g.generate_sql()
    g.write('query.sql')
    g.clear()
