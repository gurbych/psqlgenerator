CREATE TABLE "tag" (
    tag_id serial primary key,
    tag_value varchar(50) NOT NULL,
    tag_created TIMESTAMP NOT NULL DEFAULT NOW(),
    tag_updated TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE "category" (
    category_id serial primary key,
    category_title varchar(50) NOT NULL,
    category_created TIMESTAMP NOT NULL DEFAULT NOW(),
    category_updated TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE "article__tag" (
    "article_id" INTEGER NOT NULL,
    "tag_id" INTEGER NOT NULL,
    PRIMARY KEY ("article_id", "tag_id")
);

CREATE TABLE "article" (
    article_id serial primary key,
    article_text text NOT NULL,
    article_title varchar(50) NOT NULL,
    article_created TIMESTAMP NOT NULL DEFAULT NOW(),
    article_updated TIMESTAMP NOT NULL DEFAULT NOW()
);

ALTER TABLE "article__tag" ADD "article_id" INTEGER NOT NULL,
ADD CONSTRAINT "fk_article__tag_article_id" FOREIGN KEY ("article_id") REFERENCES "article" ("article_id");

ALTER TABLE "article__tag" ADD "tag_id" INTEGER NOT NULL,
ADD CONSTRAINT "fk_article__tag_tag_id" FOREIGN KEY ("tag_id") REFERENCES "tag" ("tag_id");

ALTER TABLE "article" ADD "category_id" INTEGER NOT NULL,
ADD CONSTRAINT "fk_article_category_id" FOREIGN KEY ("category_id") REFERENCES "category" ("category_id");

CREATE OR REPLACE FUNCTION update_category_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.category_updated = NOW();
    RETURN NEW;
END;
$$ language ''plpgsql'';
CREATE TRIGGER "tr_category_updated" BEFORE UPDATE ON category FOR EACH ROW EXECUTE PROCEDURE update_category_timestamp();

CREATE OR REPLACE FUNCTION update_article_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.article_updated = NOW();
    RETURN NEW;
END;
$$ language ''plpgsql'';
CREATE TRIGGER "tr_article_updated" BEFORE UPDATE ON article FOR EACH ROW EXECUTE PROCEDURE update_article_timestamp();

CREATE OR REPLACE FUNCTION update_tag_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.tag_updated = NOW();
    RETURN NEW;
END;
$$ language ''plpgsql'';
CREATE TRIGGER "tr_tag_updated" BEFORE UPDATE ON tag FOR EACH ROW EXECUTE PROCEDURE update_tag_timestamp();
