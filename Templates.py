CREATE_TABLE = """CREATE TABLE "{table}" (
    {table}_id serial primary key,
    {columns}
    {table}_created TIMESTAMP NOT NULL DEFAULT NOW(),
    {table}_updated TIMESTAMP NOT NULL DEFAULT NOW()
);
"""

CREATE_RELATION_TABLE = """CREATE TABLE "{left_table}__{right_table}" (
    "{left_table}_id" INTEGER NOT NULL,
    "{right_table}_id" INTEGER NOT NULL,
    PRIMARY KEY ("{left_table}_id", "{right_table}_id")
);
"""

CREATE_TRIGGER = """CREATE OR REPLACE FUNCTION update_{0}_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.{0}_updated = NOW();
    RETURN NEW;
END;
$$ language ''plpgsql'';
CREATE TRIGGER "tr_{0}_updated" BEFORE UPDATE ON {0} FOR EACH ROW EXECUTE PROCEDURE update_{0}_timestamp();
"""

ALTER_TABLE = """ALTER TABLE "{this}" ADD "{referenced}_id" INTEGER NOT NULL,
ADD CONSTRAINT "fk_{this}_{referenced}_id" FOREIGN KEY ("{referenced}_id") REFERENCES "{referenced}" ("{referenced}_id");
"""
